package state;


public class StateStore {

    private static StateStore instance;

    private State currentState;
    private Reducer<State> reducer;



    private StateStore() {
        currentState = new State();
        reducer = new PoorReducer();
    }

    public void updateInValues(String action, int value) {
        currentState = reducer.reduce(currentState, action, value);

        //We need to notify all interested partys that our state has changed!
    }
    

    public State getState() {
        return currentState;
    }

    public static synchronized StateStore getInstance() {
        if (instance == null) {
            instance = new StateStore();
        }

        return instance;
    }
}
