package controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import state.StateStore;
import state.State;


public class OutputTableController implements Initializable, IControllerWithLifeCycle {

    @FXML
    private Label lblWinning;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        StateStore stateStore = StateStore.getInstance();

        State state = stateStore.getState();

        lblWinning.setText(String.valueOf(state.getOutWinningCalc()));
        
    }

    @Override
    public void willUnmount() {
        System.out.println("Will unmount!!");
    }
}
