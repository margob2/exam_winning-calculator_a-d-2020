package controllers;

public interface IControllerWithLifeCycle {
    void willUnmount();
}
